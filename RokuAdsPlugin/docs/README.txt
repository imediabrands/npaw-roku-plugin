Roku Advertising Framework.  
This is a universal video ad solution integrated directly into the core Roku SDK.  
This framework natively integrates baseline & advanced advertising capabilities.
Here are few examples for integrating RAF to channel ad measurement support
in a simple channel based on list and video screen.

I. Description
	1. Full RAF integration
	
		At first configure (initialize) RAF, turn on ad measurement and configure it with documented parameters genre, length, and program ID.
	Then configure ad url with some passed parameters via Macros and set Ad URL with setAdUrl() method. After that
	get ads with getAds() (in example used VAST format) and render them with showAds().

	2. Custom Ad Parsing
	
	   In this implementation ads are imported from non-standard feed (neither VMAP, VAST or SMartXML).
	RAF is configured in a standard way, with backfill ads disabled and extra debug output enabled.	Ads are parsed
	from local JSON file, then formatted as ad pods array and imported into RAF using importAds() method.
	   After that check for particular ads to play by passing fake video events created with createPlayPosMsg()
	to RAF getAds() method before event-loop (preroll ads) and inside it (midroll/postroll ads). If any ads were
	returned from getAds(), they are rendered using RAF showAds() method.
	
	3. Stitched Ads: Mixed
	
		At first configure (initialize) RAF. Ads are parsed from local JSON file, then formatted as ad pods array (normally, ads will be populated 
	from publisher's ad URL using setAdUrl and getAds function). Then initialize this server-stitched ads using stitchedAdsInit function.
		After initialization video event loop was started. At first check if stitched ad was rendered. For this use a 
	stitchedAdHandledEvent function passing it video player message from message port and video player context. If there is no current ad 
	or ad did not handle event, the video player message fall through to default event handling.
